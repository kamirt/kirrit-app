const jsonServer = require('json-server')
const path = require('path')

const server = jsonServer.create()

const router = jsonServer.router(require(path.join(__dirname, 'db.json')))
// const router = jsonServer.router('routes.json')
const middlewares = jsonServer.defaults()
server.use(middlewares)

// handlers
const listHandler = require('./handlers/listHandler')

const port = 3000

const handlers = {
  list: listHandler
}

Object.keys(handlers).forEach((pathName) => {
  server.all(`/api/${pathName}`, (req, res, next) => {
    return handlers[pathName]({
      ...req,
      pathName
    }, res, next)
  })
})

server.use(jsonServer.bodyParser)
server.use((req, res, next) => {
  if (req.method === 'POST') {
    req.body.createdAt = Date.now()
  }
  // Continue to JSON Server router
  next()
})

// Use default router
server.use(router)

server.listen(port, () => {
  console.log(`JSON Server is running on port ${port}`)
})
