module.exports = (req, data) => {
  const DEFAULT_SIZE = 10

  const { query: { size = DEFAULT_SIZE, offset = 0 } } = req
  const sizeInt = parseInt(size)
  const offsetInt = parseInt(offset)
  const lastItem = offsetInt * sizeInt + sizeInt
  return {
    lastPage: data.length <= lastItem,
    firstPage: offsetInt === 0,
    data: data.slice(sizeInt * offsetInt, lastItem),
    totalCount: data.length
  }
}
