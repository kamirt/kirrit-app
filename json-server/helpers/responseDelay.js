const DELAY_TIME = 500

module.exports = (res, json, delay = DELAY_TIME) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(res.json(json))
    }, delay)
  })
}
