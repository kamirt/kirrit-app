const pagination = require('../helpers/pagination')
const responseDelay = require('../helpers/responseDelay')

const listTotalCount = 128

module.exports = (req, res) => {
  const data = []
  for (let i = 0; i < listTotalCount; i++) {
    data.push({
      name: `Name-${i + 1}`,
      id: i + 1
    })
  }
  return responseDelay(res, pagination(req, data))
}
