const axios = require('axios')

const apiModes = {
  development: 'http://localhost:3000/api/',
  test: 'http://localhost:3000/api/',
  production: '/api/'
}

const mode = process.env.NODE_ENV

const { [mode]: apiMode } = apiModes

const Fetcher = axios.create({
  baseURL: apiMode,
  headers: {
    'Content-Type': 'application/json'
  }
})

export default Fetcher
